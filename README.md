
![GitLab](https://www.google.com/url?sa=i&url=https%3A%2F%2Fgetfreshnews.com%2F2020%2F10%2F24%2Fbusiness-operations-tech-stack-details%2F&psig=AOvVaw2bsiArfnm1g8QMT_41Wooh&ust=1643065156274000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCIi34e_8yPUCFQAAAAAdAAAAABAO) 

# Creación y subida de repositorio en [GitLAB](https://about.gitlab.com/)

## Creación de archivos

  El primer paso es crear la carpeta "repositorio", y crear nuestro archivo index.html y nuestro archivo index.js para su posterior subida.

  ## Añadir el proyecto al control de versiones de git una vez dentro del directorio

  `$ git init`

  ## Establecer nuestro usuario y direccion de correo

   `$ git config --global user.email "mi_correo@mail.com"` 
    
  `$ git config --global user.name "nombre_usuario"`



  ## A continuación comprobar el estado de nuestro repositorio

  `$ git status`

  ## El seguiente paso será añadir nuestros archivos al área de ensayos

  `git add index.html index.js`

## Registrar los cambios en el historial

`git commit -m "Comentario del commit"`

## Comprobamos que todo esté bien

`$ git status`

## Para confirmar los cambios

`$ git remote add origin https://gitlab.com/adalidtacubeno/mi_primer_proyecto.git`


`$ git remote -v`

`$ git push -u origin --all`

En este último paso, nos pedirá que nos autentiquemos con nuestro usuario y contraseña que previamente hemos creado o modificado en nuestra cuenta de [GitLab](https://about.gitlab.com/), si la autenticación a sido correcta nos habrá subido nuestro repositorio y nos debe aparecer por consola que todo ha ido bien.

Otra opción para subir nuestro repositorio, en caso de no querer hacerlo con usuario y contraseña de [GitLab](https://about.gitlab.com/), es mediante SSH keys, en la misma web se explican los pasos para ello.



